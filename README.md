# Louvain clustering algorithm (Python)
---
## Description
The Louvain method is a simple, efficient and easy-to-implement method for identifying communities in large networks. This is a greedy optimization method that attempts to optimize the "modularity" of a partition of the network (modularity is defined [here](https://en.wikipedia.org/wiki/Louvain_Modularity)). 

The implementation corresponds to [original article](https://arxiv.org/abs/0803.0476v2).

## Method
The algorithm is:
1. each node - a singleton cluster;
2. optimizing modularity locally - vertices move sequentially to one of the communities with which they have edges and  the modularity gain is maximized;
3. step 2 is continued until cluster membership changes;
4. aggregates nodes belonging to the same community and builds a new network whose nodes are the communities.

![alt-текст](https://d0819178-a-62cb3a1a-s-sites.googlegroups.com/site/findcommunities/pol.jpg?attachauth=ANoY7cpAXR_SrVQIsiGput_003sEqomViPeKjSXlevDjKQ-4k6C0KeSzndBEH4ucGNaXD3zWu0scZrITSB23OSoPNJMFXfHzM-EsP9D4HqA2uT1DH6nSuFVOXOxwZrFCvnjC3a5Z8nTZsYA8BwxMUEdmSdw_S6lJlFle2H5MDlshiMdiNfiAOwm9zTBKsh0Ok0g7cbf_ZQ0m0D0B4clQekBzfbqbGjNr2w%3D%3D&attredirects=0)

## Installation
Clone the repository and then use this command: 
```
python Louvain.py <file to read> <file to write>
```
To run tests use this command:
```
python test.py
```
You can use some example .txt files in the repository to try the algorithm. 

## Input and output formats
 ### Input:
 Each string in the input file shoul look like: `<node name> <node's neighbout name> <weight of the edge>`
 If the graph is unweighted don't put <weigth>.
 ### Output:
 The output file contains numbers of clusters (communities) with nodes in them. Each string has this format: 
 `<number of cluster> [<list of the nodes in it>]`