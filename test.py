import subprocess
import sys
import hashlib

def compare(file_one, file_two, num):
    hash1 = hashlib.md5(open(file_one, 'rb').read()).hexdigest()
    hash2 = hashlib.md5(open(file_two, 'rb').read()).hexdigest()
    result = (hash1 == hash2)

    print_result(result, num)

def print_result(flag, num):
    if flag:
        print("Test {}: OK".format(num))
    else:
        print("Test {}: False".format(num))

def test():
    for i in range(0, 8):
        test_dat = "tests/test" + str(i) + ".dat"
        out = "out" + str(i) + ".txt"
        test_ans = "tests/test" + str(i) + ".ans"
        process = subprocess.Popen([sys.executable, "Louvain.py", test_dat, out],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd, stdout = process.communicate()
        if i <= 2:
            compare(out, test_ans, i)
        else:
            result = cmd.decode('ascii').rstrip()
            if i == 3:
                flag = (result == "Entered weight isn`t int.")
            if i == 4:
                flag = (result == "Entered incorrect edge '999'.")
            if i == 5:
                flag = (result == "Edge 0 5 doesn`t have weight.")
            if i == 6:
                flag = (result == "Edge 2 5 has weight while grahp isn`t weighted.")
            if i == 7:
                flag = (result == "Edge 4 2 has been defined twice.")

            print_result(flag, i)


if __name__ == "__main__":
    test()
