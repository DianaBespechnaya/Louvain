import os.path
import sys


class Louvain:
    def __init__(self, elements, flag):
        ''' Инициализируем рёбра, параметры модулярности - sum_edges_w, inc_edges_w, список соседей.
        Аргументы: список вершин с соседями и в случае взвешенного графа 
        (flag = 1) - вес '''
        self.sum_edges_w = 0  # Суммарный вес ребер или в случае невзвешенного - их количество
        nodes = set()
        edges = {}  # Изначально - словарь для проверки на совпадение введенных рёбер
        for line in elements:
            element = line.split()
            if len(element) == 0:
                continue
            if len(element) == 1 or len(element) > 3:
                raise Exception("Entered incorrect edge '{}'.".format(line.rstrip()))
            if flag:
                if len(element) == 3:
                    weight = int(element[2])
                else:
                    raise Exception("Edge {} {} doesn`t have weight.".format(element[0], element[1]))
            else:
                if len(element) == 2:
                    weight = 1
                else:
                    raise Exception("Edge {} {} has weight while grahp isn`t weighted.".format(element[0], element[1]))
            nodes.add(element[0])
            nodes.add(element[1])
            if (element[0], element[1]) in edges or (element[1], element[0]) in edges:
                raise Exception("Edge {} {} has been defined twice.".format(element[0], element[1]))
            else:
                edges[(element[0], element[1])] = weight
            self.sum_edges_w += weight

        self.edges = [[k[0], k[1], v] for k, v in edges.items()]
        self.n = len(nodes)  # Количество вершин
        self.inc_edges_w = [0 for n in range(self.n)]  # Список весов ребер инцидентных i-ой вершине

        self.renumber(nodes)
        self.neighbs_of_node = {}
        for e in self.edges:
            if e[0] not in self.neighbs_of_node:
                self.neighbs_of_node[e[0]] = [[e[1], e[2]]]
            else:
                self.neighbs_of_node[e[0]].append([e[1], e[2]])
            if e[1] not in self.neighbs_of_node:
                self.neighbs_of_node[e[1]] = [[e[0], e[2]]]
            else:
                self.neighbs_of_node[e[1]].append([e[0], e[2]])
            self.inc_edges_w[e[0]] += e[2]
            self.inc_edges_w[e[1]] += e[2]

        self.loops_w = [0 for n in range(self.n)]  # Список весов петель
        self.curr_clusters = [i for i in range(self.n)]  # Список текущего расположения вершин по кластерам
        self.input_graph_clusters = []  # Список расположения вершин начального графа по кластерам

    def Louvain_method(self):
        q_max = -1
        #q_max = self.modularity(self.curr_clusters)
        #print("Начальная модулярность ", q_max)
        while True:
            new_clustered_graph = self.cluster_graph()
            q = self.modularity(new_clustered_graph)
            #print("Изменение модулярности до ", q)
            if q == q_max:
                #print(self.input_graph_cluster, q)
                break
            q_max = q
            # Убираем пустые кластеры
            new_clustered_graph = [cluster for cluster in new_clustered_graph if cluster]
            self.n = len(new_clustered_graph)
            self.make_multigraph()

        result = {}
        for i in range(len(self.input_graph_clusters)):
            if self.input_graph_clusters[i] in result:
                result[self.input_graph_clusters[i]].append(self.num_dict[i])
            else:
                result[self.input_graph_clusters[i]] = [self.num_dict[i]]
        return result

    def cluster_graph(self):
        # Начальная инициализация links_in_cl и links_in_out
        self.links_in_cl = [0 for node in range(self.n)]
        self.links_out_cl = [self.inc_edges_w[node] for node in range(self.n)]
        new_clusters = [[node] for node in range(self.n)]
        # links_in_cl дважды учитывает вес петли, т.к. это вес связей внутри кластера
        for edge in self.edges:
            if edge[0] == edge[1]:
                self.links_in_cl[edge[0]] += edge[2]
                self.links_in_cl[edge[1]] += edge[2]
        # Пробуем улучшить модулярность
        while True:
            changed = 0

            for node in range(self.n):
                node_cluster = self.curr_clusters[node]
                delta_max = 0  # Максимальное изменение модулярности
                best_cluster = node_cluster  # По умолчанию лучший кластер - начальный
                # Удаляем вершину из её кластера
                new_clusters[node_cluster].remove(node)

                best_links = 0  # Количество связей node в лучшем кластере

                neighbs_cl = {}  # Словарь кластеров соседей - key, со связями других соседей - value

                for e in self.neighbs_of_node[node]:
                    neighber_cluster = self.curr_clusters[e[0]]
                    if neighber_cluster in neighbs_cl:
                        neighbs_cl[neighber_cluster] += e[1]  # Увеличиваем число связей кластера с соседями
                    else:
                        neighbs_cl[neighber_cluster] = e[1]
                    if self.curr_clusters[e[0]] == node_cluster:
                        best_links += e[1]

                self.links_in_cl[node_cluster] -= 2 * (best_links + self.loops_w[node])  # Связь внутри дважды учитывает петлю и рёбра между новой вершиной и внутренними
                self.links_out_cl[node_cluster] -= self.inc_edges_w[node]
                # Ищем лучшее сообщество для node

                for neighber_cluster in neighbs_cl.items():
                    cluster = neighber_cluster[0]
                    links = neighber_cluster[1]
                    delta = self.delta_modularity(node, cluster, links)  # Вычисляем изменение модулярности при добавлении в этот кластер
                    if delta > delta_max:
                        delta_max = delta
                        best_cluster = cluster
                        best_links = links
                # Помещаем node в лучшее для нее сообщество
                new_clusters[best_cluster].append(node)
                self.curr_clusters[node] = best_cluster
                self.links_in_cl[best_cluster] += 2 * (best_links + self.loops_w[node])
                self.links_out_cl[best_cluster] += self.inc_edges_w[node]

                if node_cluster != best_cluster:
                    changed = 1

            if not changed:
                break

        return new_clusters

    def make_multigraph(self):
        # Сжимаем граф в мультиграф, вершины - кластеры
        edges = {}
        new_clusters = []
        added = {}
        i = 0
        for cl in self.curr_clusters:  # Перенумеруем кластеры в new_clusters
            if cl not in added:
                added[cl] = i
                i += 1
            new_clusters.append(added[cl])

        for e in self.edges:  # Зададим словарь новых рёбер, вершины - кластеры
            Ci = new_clusters[e[0]]
            Cj = new_clusters[e[1]]
            if (Ci, Cj) in edges:
                edges[(Ci, Cj)] += e[2]
            elif (Cj, Ci) in edges:
                edges[(Cj, Ci)] += e[2]
            else:
                edges[(Ci, Cj)] = e[2]

        self.inc_edges_w = [0 for n in range(self.n)]
        self.loops_w = [0 for n in range(self.n)]
        self.neighbs_of_node = {}
        self.edges = [[k[0], k[1], v] for k, v in edges.items()]

        for e in self.edges:  # Зададим inc_edges_w, loops_w и neighbs_of_node для нового графа
            self.inc_edges_w[e[0]] += e[2]
            self.inc_edges_w[e[1]] += e[2]
            if e[0] == e[1]:  # Петля
                self.loops_w[e[0]] += e[2]  # Увеличиваем вес этой петли
            else:
                if e[0] not in self.neighbs_of_node:
                    self.neighbs_of_node[e[0]] = [[e[1], e[2]]]
                else:
                    self.neighbs_of_node[e[0]].append([e[1], e[2]])
                if e[1] not in self.neighbs_of_node:
                    self.neighbs_of_node[e[1]] = [[e[0], e[2]]]
                else:
                    self.neighbs_of_node[e[1]].append([e[0], e[2]])

        added = {}

        for i in range(len(new_clusters)):  # В словарь added добавляем смену вершинами кластеров
            if i != new_clusters[i]:
                added[i] = new_clusters[i]

        if len(self.input_graph_clusters) != 0:  # Применим изменения в распределении кластеров на начальный граф
            for i in range(len(self.input_graph_clusters)):
                if self.input_graph_clusters[i] in added:
                    self.input_graph_clusters[i] = added[self.input_graph_clusters[i]]
        else:
            self.input_graph_clusters = new_clusters

        self.curr_clusters = [node for node in range(self.n)]

    def delta_modularity(self, node, cluster, links):
        # delta Q = 1/(2m) * (k_i,in - sum_tot*k_i/(2m)) , sum_edges_w - вес связей
        # m = sum_edges_w; k_i,in = links; sum_tot = links_out_cl; k_i = inc_edges_w
        return links - self.links_out_cl[cluster]*self.inc_edges_w[node] / (2 * self.sum_edges_w)

    def modularity(self, clusters):
        # Q = 1/(2m)* sum(sum_in - (sum_tot/(2m))^2)
        Q = 0
        for i in range(len(clusters)):
            Q += self.links_in_cl[i] / (2 * self.sum_edges_w) - (self.links_out_cl[i] / (2 * self.sum_edges_w)) ** 2
        return Q

    # Перенумеруем узлы для быстрого доступа по индексу
    def renumber(self, nodes):
        self.num_dict = {}  # Словарь соотвествий ключ - порядковый номер
        nodes_list = []

        for n in range(self.n):
            node = nodes.pop()
            nodes_list.append(node)

        nodes_list.sort()

        for n in range(self.n):
            self.num_dict[nodes_list[n]] = n

        num_edges = []

        for e in self.edges:
            num_edges.append([self.num_dict[e[0]], self.num_dict[e[1]], e[2]])

        self.edges = num_edges
        self.num_dict = {v: k for k, v in self.num_dict.items()}


def reading(file_read):
    if not os.path.isfile(file_read):
        return "File doesn`t exist."
    f = open(file_read, 'r')
    line1 = f.readline()
    line1_list = line1.split()
    if len(line1_list) == 2:
        flag = 0
    else:
        flag = 1
    f.seek(0, 0)
    lines = f.readlines()
    f.close()

    return flag, lines


def main():
    fread = sys.argv[1]
    fwrite = sys.argv[2]
    result = reading(fread)
    if len(result) != 2:
        print(result)
        return
    weight = result[0]
    input = result[1]
    try:
        lvn = Louvain(input, weight)

        clusters = lvn.Louvain_method()
        file_write = open(fwrite, 'w')
        for key in clusters.keys():
            file_write.write("{} : {}\n".format(key, clusters[key]))
        file_write.close()

    except ValueError:
        print("Entered weight isn`t int.")
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()


